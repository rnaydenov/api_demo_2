const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

const port = process.env.PORT || 3000;      


app.listen(port, () => {
    console.log('Your app is listening to port ', port);
});


app.get('/', (req, res) => {
    res.send('Hi!');
})



